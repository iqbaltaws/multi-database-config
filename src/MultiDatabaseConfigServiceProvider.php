<?php
namespace Iqbaltaws\MultiDatabaseConfig;

use Illuminate\Support\ServiceProvider;

class MultiDatabaseConfigServiceProvider extends ServiceProvider {
	/**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
        $this->publishMigrations();
        $this->setupRoutes();
        $this->setupViews();
    }

    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

    private function publishConfig()
    {
        $this->publishes([__DIR__ . '/config/database.php' => config_path('database.php')]);
    }


    private function publishMigrations()
    {
        //for migration
    }


    private function setupRoutes()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/Http/routes.php';
        }
    }

    private function setupViews()
    {
        $appViewPath = base_path('resources/views/modules/Iqbaltaws/multi-database-config');
        $moduleViewPath = realpath(__DIR__ . '/resources/views');
        $this->loadViewsFrom([$appViewPath, $moduleViewPath], 'multi-database-config');
    }
}
